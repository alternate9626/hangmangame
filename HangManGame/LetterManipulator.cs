﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangManGame
{
    class LetterManipulator
    {
        private String word;
        public Char[] listOfUsedLetters { private set; get; }       
        private int listOfUsedLettersCounter = 0;
        private List<int> indexesOfCorrectLetters;
        private ListOfIndexesGetter listOfIndexesGetter;
        private UserInterface ui;

        public LetterManipulator(UserInterface ui,String word, ListOfIndexesGetter listOfIndexesGetter)
        {
            this.ui = ui;
            this.word = word;
            this.listOfIndexesGetter = listOfIndexesGetter;
            this.listOfUsedLetters = new Char[word.Length + 6];
            this.indexesOfCorrectLetters = listOfIndexesGetter.Get(word,listOfUsedLetters);
        }

        public bool HasLetterBeenUsed(Char letter)
        {
            return listOfUsedLetters.Contains(letter);
        }

        public bool IsLetterCorrect(Char letter)
        {
            return word.Contains(letter.ToString().ToUpper());
        }

       public  void AddLetterToListOfUsedLetter(Char letter)
        {
            listOfUsedLetters[listOfUsedLettersCounter] = letter;
            listOfUsedLettersCounter++;
            this.indexesOfCorrectLetters = listOfIndexesGetter.Get(word, listOfUsedLetters);
        }

       public void DisplayPartialWord()
       {          
           ui.ShowPartialWordBasedOnIndexesOfCorrectLetters(this.indexesOfCorrectLetters, word);
       }

       public bool IsWordCompleted()
       {
           return this.indexesOfCorrectLetters.Count() == word.Length;
       }     

    }
}

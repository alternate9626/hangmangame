﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangManGame
{
    class HangManGame
    {
        private UserInterface ui;
        private int numberOfTries = 6;
        private String randomWord;        
        private Char letter;
        private LetterManipulator letterManipulator;           

        public HangManGame(UserInterface ui, String randomWord, LetterManipulator letterManipulator)
        {
            this.ui = ui;            
            this.randomWord = randomWord;            
            this.letterManipulator = letterManipulator;    
        }

        public void Start()
        {             
            ui.ShowWelcomeMessage();
            letterManipulator.DisplayPartialWord();
        }

        public bool IsAbleToPlay()
        {
            return HasTriesLeft() && !letterManipulator.IsWordCompleted();
        }

        public void Play()
        {
            letter = ui.GetLetterFromUser();     

            if (letterManipulator.HasLetterBeenUsed(letter))
            {
                ui.ShowLetterAlreadyUsedMessage();               
            }
            else
            {
                letterManipulator.AddLetterToListOfUsedLetter(letter);               
                ProcessLetter();
                CheckForEndGameStatus();
            }
            letterManipulator.DisplayPartialWord();
        }  

        private bool HasTriesLeft()
        {
            return this.numberOfTries > 0;
        }         

        private void ProcessLetter()
        {
            if (letterManipulator.IsLetterCorrect(letter))
            {
                ui.ShowCorrectLetterMessage();
            }
            else 
            {
                numberOfTries--;
                ui.ShowTriesLeftAfterWrongLetter(numberOfTries);                
            }
        }

        private void CheckForEndGameStatus()
        {
            if (letterManipulator.IsWordCompleted())
            {
                ui.ShowWinnerMessage();
                ui.ShowWord(randomWord);
                ui.ShowPressAnyKeyToExit();
            }
            else if (!HasTriesLeft())
            {
                ui.ShowYouLostMessage();
                ui.ShowWord(randomWord);
                ui.ShowPressAnyKeyToExit();
            }
        } 
    }
}

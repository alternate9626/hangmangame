﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangManGame
{
    class HangManGameStarter
    {
        static void Main(string[] args)
        {
            UserInterface ui = new ConsoleUserInterface();
            RandomWordGetter randomWordGetter = new RandomWordGetter();
            String randomWord = randomWordGetter.Get();                        
            ListOfIndexesGetter listOfIndexesGetter = new ListOfIndexesGetter();
            LetterManipulator letterManipulator = new LetterManipulator(ui,randomWord,listOfIndexesGetter);           
            HangManGame HangManGame = new HangManGame(ui, randomWord, letterManipulator);
            HangManGame.Start();
            while (HangManGame.IsAbleToPlay())
            {
                HangManGame.Play();
            }
           
        }
    }
}

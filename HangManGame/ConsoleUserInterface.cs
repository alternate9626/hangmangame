﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangManGame
{
    class ConsoleUserInterface : UserInterface
    {
        public void AskForALetter()
        {
            Console.WriteLine("Enter a letter: ");
        }

        public String ShowPressAnyKeyToExit()
        {
            Console.WriteLine("Thanks for playing! Press any key then enter to exit.");
            return Console.ReadLine();
        }

        public Char GetLetterFromUser()
        {
            this.AskForALetter();            
            Char input = Console.ReadLine().ToString().ToUpper()[0];           
            this.ClearScreen();
            return input;
        }

        public void ShowTriesLeftAfterWrongLetter(int numberOfTries)
        {
            this.ClearScreen();
            Console.WriteLine("WRONG! You got " + numberOfTries + " left.");
        }       

        public void ShowPartialWordBasedOnIndexesOfCorrectLetters(List<int> indexesOfCorrectLetters, String word)
        {            
            for (int wordCounter = 0; wordCounter < word.Length; wordCounter++)
            {
                if (indexesOfCorrectLetters.Contains(wordCounter))
                {
                    Console.Write(word[wordCounter] + " ");
                }
                else
                {
                    Console.Write("_ ");
                } 
            }
            Console.WriteLine();
        }

        public void ShowLetterAlreadyUsedMessage()
        {
            this.ClearScreen();
            Console.WriteLine("Letter already used!");
        }

        public void ShowWinnerMessage()
        {
            Console.WriteLine("You've won!");
        }

        public void ShowWord(String word)
        {
            Console.WriteLine("The word was: " + word);
        }

        public void ClearScreen()
        {
            Console.Clear();
        }

        public void ShowYouLostMessage()
        {
            Console.WriteLine("You have lost!");
        }

        public void ShowWelcomeMessage()
        {
            Console.WriteLine("Welcome to the BEST HANGMAN GAME EVER CREATED!");
            Console.WriteLine("Brought to you by Vlad ;)");
            Console.WriteLine("------------------------------------------------");
        }

        public void ShowCorrectLetterMessage()
        {
            Console.WriteLine("Way to go! You guessed a letter!");
        }        
    }
}

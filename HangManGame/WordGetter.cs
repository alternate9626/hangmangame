﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangManGame
{
    class RandomWordGetter{ 

        Random rand = new Random();

        public String Get()
        {
            return GetRandomWord();
        }

        public String GetRandomWord()
        {            
            int randomNumber = rand.Next(20);
            String[] randomWords = new String[20];

            randomWords[0] = "HOUSE";
            randomWords[1] = "DOG";
            randomWords[2] = "CAT";
            randomWords[3] = "FLOOR";
            randomWords[4] = "LAKE";
            randomWords[5] = "JOB";
            randomWords[6] = "CODE";
            randomWords[7] = "WORK";
            randomWords[8] = "NOSE";
            randomWords[9] = "BOOK";
            randomWords[10] = "RED";
            randomWords[11] = "CLOCK";
            randomWords[12] = "ZOO";
            randomWords[13] = "RANDOM";
            randomWords[14] = "PAGE";
            randomWords[15] = "FANCY";
            randomWords[16] = "STUFF";
            randomWords[17] = "ONE";
            randomWords[18] = "SKYPE";
            randomWords[19] = "COMPUTER";

            return randomWords[randomNumber];           
        }
    }
}




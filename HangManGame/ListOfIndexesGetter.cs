﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HangManGame
{
    class ListOfIndexesGetter
    {
        public List<int> Get(String word, Char[] listOfUsedLetters)
        {
            List<int> indexesOfCorrectLetters = new List<int>();
            for (int counter = 0; counter < word.Length; counter++)
            {
                if (listOfUsedLetters.Contains(word[counter]))
                {
                    indexesOfCorrectLetters.Add(counter);
                }
            }
            return indexesOfCorrectLetters;
        }
    }
}

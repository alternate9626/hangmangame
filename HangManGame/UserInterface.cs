﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangManGame
{
    interface UserInterface
    {
        void AskForALetter();

        String ShowPressAnyKeyToExit();            

        Char GetLetterFromUser();

        void ShowTriesLeftAfterWrongLetter(int numberOfTries);        

        void ShowPartialWordBasedOnIndexesOfCorrectLetters(List<int> indexesOfCorrectLetters, String word);

        void ShowLetterAlreadyUsedMessage();

        void ShowWinnerMessage();

        void ShowWord(String word);

        void ClearScreen();

        void ShowYouLostMessage();

        void ShowWelcomeMessage();

        void ShowCorrectLetterMessage();
        
    }
}
